@extends('layout.template')
@section('titulo')
    Tareas - Admin
@endsection

@section('header')
<header class="masthead" style="background-image: url({{asset('style/img/about-bg.jpg')}}">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-10 mx-auto" style="text-align: center;">
                <div class="site-heading">
                    <h3>Crear Tareas</h3>
                </div>
            </div>
        </div>
    </div>
</header>
@endsection

@section('contenido')
    <section class="container">

        <div class="row">
            <div class="col-12">
                <div class="info-box">
                {!! Form::open(['route' => 'storeTareas', 'method' => 'POST']) !!}
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="tituloTareas">Titulo <span class="text-danger">*</span></label>
                            {!! Form::text('tituloTareas', null, ['class' => 'form-control', 'required', 'autocomplete' => 'off', 'placeholder' => 'Titulo'])!!}
                            @if($errors->has('tituloTareas'))
                                <small class="text-danger">{{$errors->first('tituloTareas')}}</small>
                            @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('estadoTareas') ? 'has-error' : '' }}">
                            <label for="estadoTareas">Estado <span class="text-danger">*</span></label>
                            <select class="form-control" name="estadoTareas">
                                <option value="1">Activo</option>
                                <option value="2">Inactivo</option>
                            </select>
                            @if($errors->has('estadoTareas'))
                                <small class="text-danger">{{$errors->first('estadoTareas')}}</small>
                            @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                            <label for="descripcionTareas">Descripcion </label>
                            <textarea rows="7" name="descripcionTareas" id="descripcionTareas" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                    <div class="col-12 text-center">
                        {!!Form::submit('Agregar Tarea', ['class'=>'btn btn-info margin-top-10'])!!}
                    </div>
                    </div>
                </div>
                {!! Form::close() !!}
                <!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
@endsection