@extends('layout.template')
@section('titulo')
    Tareas - Admin
@endsection

@section('header')
<header class="masthead" style="background-image: url({{asset('style/img/about-bg.jpg')}}">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-10 mx-auto" style="text-align: center;">
                <div class="site-heading">
                    <h3>Gestionar Tareas</h3>
                </div>
            </div>
        </div>
    </div>
</header>
@endsection

@section('contenido')
    <div class="container" style="text-align: center;">
        <div class="row">
            <div class="col-12">
                @if(Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{Session::get('message')}}
                    </div>
                    @elseif(Session::has('message_error'))
                    <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{Session::get('message_error')}}
                    </div>
                @endif
            </div>
            <div class="col-12 text-right">
                <a href="{{route('createTareas')}}" class="text-right btn btn-sm btn-primary"><i class="fas fa-plus"></i></a>
            </div>
            <table class="table">
                <tr>
                    <th>#</th>
                    <th>Titulo</th>
                    <th>Descripcion</th>
                    <th>Estado</th>
                    <th>Opciones</th>
                </tr>
                <?php $number = 1; ?>
                @if(count($tareas) <> 0)
                @foreach($tareas as $listTareas)
                    <tr>
                        <td>{{$number}}</td>
                        <td>{{$listTareas->tituloTareas}}</td>
                        <td>{{$listTareas->descripcionTareas}}</td>
                        <td>
                            @if($listTareas->estadoTareas == 1)
                                <span class="badge badge-success">Activo</span>
                            @else
                                <span class="badge badge-danger">Inactivo</span>
                            @endif
                        </td>
                        <td>
                            <a href="{{route('editTareas', $listTareas->idTareas)}}" class="btn btn-sm btn-primary"><i class="fas fa-edit"></i></a>
                            <a href="{{route('deleteTareas', $listTareas->idTareas)}}" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></a>
                        </td>
                    </tr>
                    <?php $number ++; ?>
                @endforeach
                @else
                <tr>
                    <td rowspan="5" colspan="5"><h3>Aun no Tienes Tareas</h3></td>
                </tr>
                @endif
            </table>
        </div>
    </div>
@endsection