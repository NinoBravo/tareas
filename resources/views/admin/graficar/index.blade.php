@extends('layout.template')
@section('titulo')
    Graficar - Admin
@endsection

@section('header')
<header class="masthead" style="background-image: url({{asset('style/img/about-bg.jpg')}}">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-10 mx-auto" style="text-align: center;">
                <div class="site-heading">
                    <h3>Graficar</h3>
                </div>
            </div>
        </div>
    </div>
</header>
@endsection

@section('contenido')
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <div id="grafica">

                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $( document ).ready(function() {
            Highcharts.setOptions({
                global: {
                    useUTC: false
                }
            });

            Highcharts.chart('grafica', {
                chart: {
                    type: 'spline',
                    animation: Highcharts.svg, // don't animate in old IE
                    marginRight: 10,
                    events: {
                        load: function () {

                            // set up the updating of the chart each second
                            var series = this.series[0];
                            setInterval(function () {
                                var x = (new Date()).getTime(), // current time
                                    y = Math.random();
                                series.addPoint([x, y], true, true);
                            }, 1000);
                        }
                    }
                },
                title: {
                    text: 'Graficar'
                },
                xAxis: {
                    type: 'datetime',
                    tickPixelInterval: 150
                },
                yAxis: {
                    title: {
                        text: 'Value'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    formatter: function () {
                        return '<b>' + this.series.name + '</b><br/>' +
                            Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
                            Highcharts.numberFormat(this.y, 2);
                    }
                },
                legend: {
                    enabled: false
                },
                exporting: {
                    enabled: false
                },
                series: [{
                    name: 'Tiempo',
                    data: (function () {
                        // generate an array of random data
                        var data = [],
                            time = (new Date()).getTime(),
                            i;

                        for (i = -19; i <= 0; i += 1) {
                            data.push({
                                x: time + i * 1000,
                                y: Math.random()
                            });
                        }
                        return data;
                    }())
                }]
            });
        });
    </script>
@endsection