@extends('layout.template')
@section('titulo')
    Mi Perfil - Admin
@endsection

@section('header')
<header class="masthead" style="background-image: url({{asset('style/img/about-bg.jpg')}}">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-10 mx-auto" style="text-align: center;">
                <div class="site-heading">
                    <h3>{{Auth::user()->nombreUsuario}} {{Auth::user()->apellidoUsuario}}</h3>
                </div>
            </div>
        </div>
    </div>
</header>
@endsection

@section('contenido')
<div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-10 mx-auto">
                <div class="site-heading">
                    @if(Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{Session::get('message')}}
                    </div>
                    @elseif(Session::has('message_error'))
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{Session::get('message_error')}}
                    </div>
                    @endif
                    {!! Form::open(['route' => 'storePerfil', 'method' => 'POST']) !!}
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="nombre">Nombre</label>
                            {!! Form::text('nombre', $dataUser->nombreUsuario, ['class' => 'form-control', 'required', 'autocomplete' => 'off', 'placeholder' => 'Nombre'])!!}
                            @if($errors->has('nombre'))
                                <small style="color: #000;">{{$errors->first('nombre')}}</small>
                            @endif
                        </div>
                        <div class="form-group col-md-6">
                            <label for="apellido">Apellido</label>
                            {!! Form::text('apellido', $dataUser->apellidoUsuario, ['class' => 'form-control', 'required', 'autocomplete' => 'off', 'placeholder' => 'Apellido'])!!}
                            @if($errors->has('apellido'))
                                <small style="color: #000;">{{$errors->first('apellido')}}</small>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="telefono">Telefono</label>
                            {!! Form::number('telefono', $dataUser->telefono, ['class' => 'form-control', 'required', 'autocomplete' => 'off', 'placeholder' => 'Telefono'])!!}
                            @if($errors->has('telefono'))
                                <small style="color: #000;">{{$errors->first('telefono')}}</small>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="password">Contraseña</label>
                            {!! Form::password('password', ['class' => 'form-control', 'autocomplete' => 'off', 'placeholder' => 'Contraseña'])!!}
                            @if($errors->has('password'))
                                <small style="color: #000;">{{$errors->first('password')}}</small>
                            @endif
                        </div>
                        <div class="form-group col-md-6">
                            <label for="password_confirmation">Repetir Contraseña</label>
                            {!! Form::password('password_confirmation', ['class' => 'form-control', 'autocomplete' => 'off', 'placeholder' => 'Repetir Password'])!!}
                            @if($errors->has('password_confirmation'))
                                <small style="color: #000;">{{$errors->first('password_confirmation')}}</small>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-center">
                            {!!Form::submit('Guardar Datos', ['class'=>'btn btn-primary margin-top-10'])!!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection