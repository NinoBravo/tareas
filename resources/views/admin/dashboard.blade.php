@extends('layout.template')
@section('titulo')
    Dashboard - Admin
@endsection

@section('header')
<header class="masthead" style="background-image: url({{asset('style/img/about-bg.jpg')}}">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-10 mx-auto">
                <div class="site-heading">
                    <h3>Bienvenido - {{Auth::user()->nombreUsuario}} {{Auth::user()->apellidoUsuario}}</h3>
                </div>
            </div>
        </div>
    </div>
</header>
@endsection