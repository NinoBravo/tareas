<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>@yield('titulo')</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('style/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="{{asset('style/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="{{asset('style/css/clean-blog.min.css')}}" rel="stylesheet">

    <link href="{{asset('style/graficas/css/highcharts.css')}}" rel="stylesheet">

  </head>

  <body>

    @include('header.nav')

    @yield('header')

    <!-- Main Content -->
    @yield('contenido')

    @include('header.footer')

    <!-- Bootstrap core JavaScript -->
    <script src="{{asset('style/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('style/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Custom scripts for this template -->
    <script src="{{asset('style/js/clean-blog.min.js')}}"></script>

    <!-- GRAFICAS -->
    <script src="{{asset('style/graficas/js/highcharts.js')}}"></script>

    @yield('js')

  </body>

</html>
