<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand" href="#">Multi Tareas</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            Menu
            <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                @if(Auth::user())
                <li class="nav-item">
                    <a class="nav-link" href="{{route('dashboardUser')}}">Inicio</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('indexGraficar')}}">Graficar</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('indexTareas')}}">Gestionar Tareas</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('indexPerfil')}}">Mi Perfil</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('logout')}}">Salir</a>
                </li>
                @else
                <li class="nav-item">
                    <a class="nav-link" href="{{route('inicio')}}">Iniciar Session</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('registrarse')}}">Registrate</a>
                </li>
                @endif
            </ul>
        </div>
    </div>
</nav>