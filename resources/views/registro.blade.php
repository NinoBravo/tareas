@extends('layout.template')
@section('titulo')
    Registrar
@endsection

@section('header')
<header class="masthead" style="background-image: url({{asset('style/img/home-bg.jpg')}}">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-10 mx-auto">
                <div class="site-heading">
                    {!! Form::open(['route' => 'storeRegistro', 'method' => 'POST']) !!}
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="nombre">Nombre</label>
                            {!! Form::text('nombre', null, ['class' => 'form-control', 'required', 'autocomplete' => 'off', 'placeholder' => 'Nombre'])!!}
                            @if($errors->has('nombre'))
                                <small style="color: #fff;">{{$errors->first('nombre')}}</small>
                            @endif
                        </div>
                        <div class="form-group col-md-6">
                            <label for="apellido">Apellido</label>
                            {!! Form::text('apellido', null, ['class' => 'form-control', 'required', 'autocomplete' => 'off', 'placeholder' => 'Apellido'])!!}
                            @if($errors->has('apellido'))
                                <small style="color: #fff;">{{$errors->first('apellido')}}</small>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="correo">Correo</label>
                            {!! Form::email('email', null, ['class' => 'form-control', 'required', 'autocomplete' => 'off', 'placeholder' => 'Ejemplo... Example@gmail.com'])!!}
                            @if($errors->has('email'))
                                <small style="color: #fff;">{{$errors->first('email')}}</small>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-7">
                            <label for="cedula">Cédula</label>
                            {!! Form::number('cedula', null, ['class' => 'form-control', 'required', 'autocomplete' => 'off', 'placeholder' => 'Cédula de ciudadanía'])!!}
                            @if($errors->has('cedula'))
                                <small style="color: #fff;">{{$errors->first('cedula')}}</small>
                            @endif
                        </div>
                        <div class="form-group col-md-5">
                            <label for="telefono">Telefono</label>
                            {!! Form::number('telefono', null, ['class' => 'form-control', 'required', 'autocomplete' => 'off', 'placeholder' => 'Telefono'])!!}
                            @if($errors->has('telefono'))
                                <small style="color: #fff;">{{$errors->first('telefono')}}</small>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="password">Contraseña</label>
                            {!! Form::password('password', ['class' => 'form-control', 'required', 'autocomplete' => 'off', 'placeholder' => 'Contraseña'])!!}
                            @if($errors->has('password'))
                                <small style="color: #fff;">{{$errors->first('password')}}</small>
                            @endif
                        </div>
                        <div class="form-group col-md-6">
                            <label for="password_confirmation">Repetir Contraseña</label>
                            {!! Form::password('password_confirmation', ['class' => 'form-control', 'required', 'autocomplete' => 'off', 'placeholder' => 'Repetir Password'])!!}
                            @if($errors->has('password_confirmation'))
                                <small style="color: #fff;">{{$errors->first('password_confirmation')}}</small>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-center">
                            {!!Form::submit('Registrarse', ['class'=>'btn btn-primary margin-top-10'])!!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</header>
@endsection