@extends('layout.template')
@section('titulo')
    Iniciar Session
@endsection

@section('header')
<header class="masthead" style="background-image: url({{asset('style/img/home-bg.jpg')}}">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-10 mx-auto">
                <div class="site-heading">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{Session::get('message')}}
                </div>
                @elseif(Session::has('message_error'))
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{Session::get('message_error')}}
                </div>
                @endif
                {!! Form::open(['route' => 'loginPost', 'method' => 'POST']) !!}
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="email">Correo</label>
                            <input type="email" name="email" class="form-control" id="email" placeholder="Correo Electronico">
                        </div>
                        <div class="form-group col-md-12">
                            <label for="password">Contraseña</label>
                            <input type="password" name="password" class="form-control" id="password" placeholder="Contraseña">
                            <div class="col-12 text-right"><a href="{{route('registrarse')}}"><small class="text-right">¿No tienes cuenta?</small></a></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-center">
                            <button type="submit" class="btn btn-info">Entrar</button>
                        </div>
                    </div>
                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</header>
@endsection