<?php


Route::get('/', ['uses' => 'InicioController@index', 'as' => 'inicio']);
Route::post('login', ['uses' => 'Auth\LoginController@loginPost', 'as' => 'loginPost']);
Route::get('registro', ['uses' => 'RegistroController@index', 'as' => 'registrarse']);
Route::post('registro/post', ['uses' => 'RegistroController@store', 'as' => 'storeRegistro']);
Route::get('logout', ['uses' => 'Auth\LoginController@getLogout', 'as' => 'logout']);


Route::group(['prefix' => 'administrator', 'middleware' => 'auth'], function (){
    Route::get('/', ['uses' => 'Admin\DashboardController@index', 'as' => 'dashboardUser']);

    //CRUD TAREAS
    Route::get('gestionar-tareas', ['uses' => 'Admin\GestionarTareasController@index', 'as' => 'indexTareas']);
    Route::get('gestionar-tareas/create', ['uses' => 'Admin\GestionarTareasController@create', 'as' => 'createTareas']);
    Route::post('gestionar-tareas/post', ['uses' => 'Admin\GestionarTareasController@store', 'as' => 'storeTareas']);
    Route::get('gestionar-tareas/edit/{id}', ['uses' => 'Admin\GestionarTareasController@edit', 'as' => 'editTareas']);
    Route::post('gestionar-tareas/edit/post', ['uses' => 'Admin\GestionarTareasController@update', 'as' => 'updateTareas']);
    Route::get('gestionar-tareas/delete/{id}', ['uses' => 'Admin\GestionarTareasController@delete', 'as' => 'deleteTareas']);

    //Graficar
    Route::get('graficar', ['uses' => 'Admin\GraficarController@index', 'as' => 'indexGraficar']);

    //Perfil
    Route::get('perfil', ['uses' => 'Admin\PerfilController@index', 'as' => 'indexPerfil']);
    Route::post('perfil/post', ['uses' => 'Admin\PerfilController@store', 'as' => 'storePerfil']);

    
});
