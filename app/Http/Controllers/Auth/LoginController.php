<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

use Illuminate\Contracts\Auth\Guard;

use Auth;
use Session;
use Redirect;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/administrator';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    public function loginPost(Request $request)
    {
      $this->validate($request, [
          'email' => 'required',
          'password' => 'required',
      ]);

      $remember = ($request->remember) ? true : false;

      $credentials = $request->only('email', 'password');

      $users = User::Where('email', $request->email)->first();

      if (!empty($users)) {
        if (Auth::attempt($credentials)) {
            return redirect::to('/administrator');
        }else{
          Session::flash('message_error', 'La Contraseña es Incorrecta');
          return redirect::to('/');
        }
      }else{
        Session::flash('message_error', 'Este Usuario No Existe... Resgistrate');
        return redirect::to('/');
      }

    }

    protected function getLogout()
    {
        Auth::logout();
        Session::flush();
        return Redirect::to('/');
    }
}
