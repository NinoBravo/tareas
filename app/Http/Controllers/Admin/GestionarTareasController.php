<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Tareas;

use Auth;
use Session;
use Redirect;

class GestionarTareasController extends Controller
{
    public function index()
    {
        $idUsuario = Auth::user()->idUsuario;
        $tareas = Tareas::where('idUsuario', $idUsuario)->where('estadoTareas', '<>', 0)->get();
        return view('admin.tareas.index',compact('tareas'));
    }

    public function create()
    {
        return view('admin.tareas.create');
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'tituloTareas' => 'required|min:3',
            'estadoTareas' => 'required',
        ],$messages = [
            'tituloTareas.required' => 'Agrega el titulo de tu Tarea.',
            'tituloTareas.min' =>'El titulo debe ser mayor a 3 Caracteres',
            'estadoTareas.required' => 'Agrega el Estado.',
        ]);

        $idUsuario = Auth::user()->idUsuario;

        $newTarea = new Tareas();
        $newTarea->idUsuario = $idUsuario;
        $newTarea->tituloTareas = $request->tituloTareas;
        $newTarea->estadoTareas = $request->estadoTareas;
        $newTarea->descripcionTareas = $request->descripcionTareas;

        if ($newTarea->save()) {
            Session::flash('message', 'Tarea creada correctamente');
            return redirect::to('/administrator/gestionar-tareas');
        }else{
            Session::flash('message_error', 'Ha ocurrido un error, intente mas tarde');
            return redirect::to('/administrator/gestionar-tareas');
        }
    }

    public function edit($id)
    {
        $tarea = Tareas::where('idTareas', $id)->first();
        return view('admin.tareas.edit', compact('tarea'));
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'tituloTareas' => 'required|min:3',
            'estadoTareas' => 'required',
        ],$messages = [
            'tituloTareas.required' => 'Agrega el titulo de tu Tarea.',
            'tituloTareas.min' =>'El titulo debe ser mayor a 3 Caracteres',
            'estadoTareas.required' => 'Agrega el Estado.',
        ]);

        $idTarea = $request->idTarea;

        $editTarea = Tareas::find($idTarea);
        $editTarea->tituloTareas = $request->tituloTareas;
        $editTarea->estadoTareas = $request->estadoTareas;
        $editTarea->descripcionTareas = $request->descripcionTareas;

        if($editTarea->save())
        {
            Session::flash('message', 'Tarea Editada correctamente');
            return redirect::to('/administrator/gestionar-tareas');
        }else
        {
            Session::flash('message_error', 'Ha ocurrido un error, intente mas tarde');
            return redirect::to('/administrator/gestionar-tareas');
        }
    }

    public function delete($id)
    {
        $tareas = Tareas::where('idTareas', $id)->first();
        $tareas->estadoTareas = 0;

        if ($tareas->save()) {
            Session::flash('message', 'Tarea eliminada correctamente');
            return redirect::to('/administrator/gestionar-tareas');
        }else{
            Session::flash('message_error', 'Ha ocurrido un error, intente mas tarde');
            return redirect::to('/administrator/gestionar-tareas');
        }
    }
}
