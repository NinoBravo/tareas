<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Tareas;
use Auth;

class GraficarController extends Controller
{
    public function index()
    {
        $idUser = Auth::user()->idUsuario;

        $tareas = Tareas::where('idUsuario', $idUser)->get();

        return view('admin.graficar.index');
    }
}
