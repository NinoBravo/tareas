<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Redirect;
use Session;

class PerfilController extends Controller
{
    public function index()
    {
        $idUser = Auth::user()->idUsuario;
        $dataUser = User::where('idUsuario', $idUser)->first();
        return view('admin.perfil',compact('dataUser'));
    }

    public function store(Request $request)
    {
        $idUser = Auth::user()->idUsuario;

        $this->validate($request, [
            'nombre' => 'required|min:4',
            'apellido' => 'required|min:4',
            'telefono' => 'required|numeric',
            'password' => 'confirmed|min:4'
        ],$messages = [
            'nombre.required' => 'El campo nombre no puede estar vacio',
            'nombre.min' =>'El campo nombre debe ser mayor a 4 caracteres',
            'apellido.required' => 'El campo apellido no puede estar vacio',
            'apellido.min' =>'El campo apellido debe ser mayor a 4 caracteres',
            'telefono.required' => 'El campo telefono no puede estar vacio',
            'telefono.numeric' => 'El campo telefono es numerico',
            'password.confirmed' => 'El campo password no son iguales',
            'password.min' =>'El campo password debe ser mayor a 4 caracteres',
            
        ]);

        $user = User::find($idUser);
        $user->nombreUsuario = $request->nombre;
        $user->apellidoUsuario = $request->apellido;
        $user->telefono = $request->telefono;
        if (!empty($request->password)) {
            $user->password = bcrypt($request->password);
        }

        if ($user->save()) {
            Session::flash('message', 'Datos Actualizados Correctamente');
            return redirect::to('administrator/perfil');
        }else{
            Session::flash('message_error', 'Ha ocurrido un error, intente mas tarde');
            return redirect::to('administrator/perfil');
        }
        
    }
}
