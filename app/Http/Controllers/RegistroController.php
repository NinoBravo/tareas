<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Session;
use App\User;
use Redirect;

class RegistroController extends Controller
{
    public function index()
    {
        return view('registro');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required|min:4',
            'apellido' => 'required|min:4',
            'email' => 'required|unique:users|email',
            'cedula' => 'required|numeric',
            'telefono' => 'required|numeric',
            'password' => 'required|confirmed|min:4'
        ],$messages = [
            'nombre.required' => 'El campo nombre no puede estar vacio',
            'nombre.min' =>'El campo nombre debe ser mayor a 4 caracteres',
            'apellido.required' => 'El campo apellido no puede estar vacio',
            'apellido.min' =>'El campo apellido debe ser mayor a 4 caracteres',
            'email.required' => 'El campo correo no puede estar vacio',
            'email.unique' => 'Este correo ya existe',
            'cedula.required' => 'El campo cedula no puede estar vacio',
            'cedula.numeric' => 'El campo cedula es numerico',
            'telefono.required' => 'El campo telefono no puede estar vacio',
            'telefono.numeric' => 'El campo telefono es numerico',
            'password.required' => 'El campo password no puede estar vacio',
            'password.confirmed' => 'El campo password no son iguales',
            'password.min' =>'El campo password debe ser mayor a 4 caracteres',
            
        ]);

        $newUser = new User();
        $newUser->nombreUsuario = $request->nombre;
        $newUser->apellidoUsuario = $request->apellido;
        $newUser->email = $request->email;
        $newUser->cedula = $request->cedula;
        $newUser->telefono = $request->telefono;
        $newUser->password = bcrypt($request->password);

        if ($newUser->save()) {
            Session::flash('message', 'Registro Exitoso, Por favor Inicia Session');
            return redirect::to('/');
        }else{
            Session::flash('message_error', 'Ha ocurrido un error, intente mas tarde');
            return redirect::to('/registro');
        }
    }
}
