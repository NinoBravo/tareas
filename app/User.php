<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $primaryKey = 'idUsuario';

    protected $fillable = [
       'nombreUsuario',
       'apellidoUsuario',
       'cedula',
       'telefono',
       'email',
       'password'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
}
